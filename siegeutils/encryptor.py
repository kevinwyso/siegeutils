from cryptography.fernet import Fernet

SEC_KEY="5D6_KJYY8ALxkhUQ7uVfpo-T6SGFooQ2ctrjoZZ4tKk="

def main():
    text_to_encrypt = input("Text to Encrypt: ")
    encrypted_text = encrypt_text(text_to_encrypt)
    print("Encrypted form: " + encrypted_text)
    print("Decrypted Check: " + decrypt_text(encrypted_text))
    
def encrypt_text(text):
    f = Fernet(SEC_KEY)
    return f.encrypt(text.encode()).decode()
    
def decrypt_text(text):
    f = Fernet(SEC_KEY)
    return f.decrypt(text.encode()).decode()

if __name__ == '__main__':
    main()